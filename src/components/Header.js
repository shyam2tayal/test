import React from "react";
import {Link} from 'react-router-dom';


const Header = () => (
    <nav>
        <ul>
            <li>
                <Link to="/">Home</Link>
                <Link to="/about">About</Link>
                <Link to="/contact">Contact</Link>
            </li>
        </ul>
    </nav>
)

export default Header;
