import React, {Suspense} from "react";
import {Switch, Route, BrowserRouter} from 'react-router-dom';
const About = React.lazy(() => import("./About"));
const Contact = React.lazy(() => import("./Contact"));
const Home = React.lazy(() => import("./Home"));

const Main = ({props}) => {


    return (
    <main>
        <Suspense fallback={() => <p>Loading</p>}>
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <Route exact path="/about" component={About}></Route>
                <Route exact path="/contact" component={Contact}></Route>
            </Switch>
        </Suspense>
    </main>
)
    }

export default Main;