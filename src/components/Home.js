import React from "react";
import store from "../Store";
import { connect} from "react-redux";

const Home = ({articles, searchTerm, onSearch}) => {

    return (
        <ErrorBoundary>
            <div>
                <p>Hello</p>
            <Search value={searchTerm} onSearch={onSearch} />
            <Articles articles={articles.filter(applyFilter(searchTerm))} />
            </div>
        </ErrorBoundary>
    )
}

const applyFilter = searchTerm => article => {
    return article.title.toLowerCase().includes(searchTerm.toLowerCase()
    )
}

const Search = ({value, onSearch, children}) => (
    <div>
            <p>Search</p>
            <input value={value} 
            onChange={event => onSearch(event.target.value)}/>

    </div>

)

const Articles = ({articles}) => 
<ul>
    {articles.map(article => 
        <li key={article.id}>
            <a href="">{article.title}</a>
        </li>
    )}
</ul>



const mapStateToProps = state => ({
    articles: state.dataState.articles,
    searchTerm: state.searchState.searchTerm
})

const mapDispatchToProps = dispatch => ({
   onSearch: searchTerm => dispatch({type: 'SEARCH_SET', searchTerm})
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);