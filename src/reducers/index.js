import { combineReducers } from "redux";

import dataReducer from "./dataReducer";
import searchReducer from "./searchReducer";


const rootReducer = combineReducers({
    dataState: dataReducer,
    searchState: searchReducer
})

export default rootReducer;