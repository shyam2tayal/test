const INITIAL_STATE = {
    articles: [
        {
            "id": 0, title: 'abc'
        },
        {
            "id": 1, title: 'def'
        },
        {
            "id": 2, title: 'ghi'
        },
        {
            "id": 3, title: 'jkl'
        },
    ]
}

function dataReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        default: return state;
    }
}

export default dataReducer;