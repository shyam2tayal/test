import './App.css';
import React from 'react';
import { Provider } from 'react-redux';
import store from './Store';
import Home from './components/Home';


function App() {
  return (
    <Provider store={store}>
      <p>Welcome</p>
      <Home />
    </Provider>
  );
}



export default App;
